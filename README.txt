Idea from https://www.carmodule.de/produkt_P19784_Bordsteinautomatik_V6.html
with very similar handling but additional features:
- forward each mirror input independently (for systems using 3 wires for the 2 motors)
- inversion of up/down

/*
Digispark Pro pins: https://digistump.com/wiki/digispark/tutorials/pinguidepro
1 - onBoard LED
3 - USB+
4 - USB-
*/


// CONFIGURATION
//#define DEBUG
#define Mirror1PinIn A6             // analog
#define Mirror2PinIn A7             // analog
#define Mirror1PinHighOut 10
#define Mirror1PinLowOut 11
#define Mirror2PinHighOut 8
#define Mirror2PinLowOut 9
#define ReversePinIn 0
#define MagneticswitchPinOut 12
#define BlockPinHighIn 2
#define BlockPinLowIn 5
#define LedPin 1                    // Digispark Pro circuit
#define HeighteningDelay 5000       // [ms]
#define LedBlinkSlow 500            // [ms]
#define LedBlinkFast 200            // [ms]


#include <avr/eeprom.h>


// PREFERENCES

#define configVersion "almir10"
#define configStart 32

struct preferencesStruct {          // default preferences
    char version[8] = configVersion;
    unsigned long loweringDuration = 1000;  // [ms]
    bool invert = false;
    bool doubleReverse = false;
} preferences;



// globals

enum State { ready, aborted, moving, almir, restore };
enum State state = ready;
enum State led = ready;
unsigned long lowered = 0;      // current position

#ifdef DEBUG
    #include <DigiCDC.h>
    void debug(const __FlashStringHelper* fmt, ...) {
        char result[55];
        int len;
        va_list valist;       // https://www.tutorialspoint.com/cprogramming/c_variable_arguments.htm
        va_start(valist, fmt);
        // https://www.tutorialspoint.com/c_standard_library/c_function_vsprintf.htm
        // https://www.microchip.com/webdoc/AVRLibcReferenceManual/group__avr__stdio_1gaf47f5141509d1e434f9da2b27287a707.html
        len = vsprintf_P(result, (const char*)fmt, valist);
        va_end(valist);
        for (unsigned int i=0; i<len; i++) {
            SerialUSB.write(result[i]);
            delayBackground(25);
        }
        SerialUSB.write("\n\r");
        delayBackground(50);
    }
#else
    #define debug(fmt, ...)
#endif

void set_state(const enum State s) {
    debug(F("set_state(%d)"), s);
    state = s;
    set_led(state);
}



// preferences

void preferences_save() {
    debug(F("preferences_save()"));
    for (unsigned int i=0; i<sizeof(preferences); i++)
        eeprom_write_byte((unsigned char *) (configStart + i), *((char*)&preferences + i));
}

void preferences_load() {
    // To make sure there are settings, and they are YOURS!
    // If nothing is found it will use the default settings.
    for (unsigned int i=0; i<sizeof(configVersion); i++) {
        if( eeprom_read_byte((unsigned char *) (configStart + i)) != configVersion[i] ) {
            debug(F("preferences_load(): no configuration '%s' found"), configVersion);
            return;
        }
    }
    debug(F("preferences_load(): restore configuration"));
    for (unsigned int i=0; i<sizeof(preferences); i++)
        *((char*)&preferences + i) = eeprom_read_byte((unsigned char *) (configStart + i));
}

void preferences_reset() {
    debug(F("preferences_reset()"));
    for (unsigned int i=0; i<sizeof(preferences); i++)
        eeprom_write_byte((unsigned char *) (configStart + i), 0);
}


// input

int read(const uint8_t pin) {
    return (analogRead(pin)*3)/1024 - 1;
}


bool input() {
    // input to at least one lane
    return read(Mirror1PinIn) != 0 || read(Mirror2PinIn) != 0;
}


bool input_down() {
    // input to both lanes
    const int mirror1 = read(Mirror1PinIn), mirror2 = read(Mirror2PinIn);
    return ( !preferences.invert && mirror1 > 0 && mirror2 < 0 )
            || ( preferences.invert && mirror1 < 0 && mirror2 > 0);
}


bool input_up() {
    // input to both lanes
    const int mirror1 = read(Mirror1PinIn), mirror2 = read(Mirror2PinIn);
    return ( !preferences.invert && mirror1 < 0 && mirror2 > 0 )
            || ( preferences.invert && mirror1 > 0 && mirror2 < 0);
}


bool reverse_gear() {
    return digitalRead(ReversePinIn) == HIGH
            && digitalRead(BlockPinHighIn) != HIGH
            && digitalRead(BlockPinLowIn) != LOW;
}


unsigned int reverse_count() {
    static unsigned long lastReverse = 0;
    static bool lastState = false;
    static unsigned int count = 0;
    const bool currentState = reverse_gear();
    const bool timeout = millis() - lastReverse > HeighteningDelay;

    if( currentState ) {
        if( count==0 || (!lastState && !timeout)) {  // no-->yes first time or in time
            count += 1;
            debug(F("reverse_count(): increment count to %d"), count);
        }
        lastReverse = millis();
    } else if( count>0 && timeout ) {
        count = 0;
        debug(F("reverse_count(): reset count to 0"));
        set_state(ready);
    }
    lastState = currentState;
 
    return count;
}


bool reverse() {
    const unsigned int count = reverse_count();       // update state
    return state != aborted
            && ( (!preferences.doubleReverse && count >= 1)
                || (preferences.doubleReverse && count >=2) );
}


// output

void forward_inputs() {
    if( read(Mirror1PinIn) > 0 ) {
        digitalWrite(Mirror1PinLowOut, LOW);
        digitalWrite(Mirror1PinHighOut, LOW);
    } else if( read(Mirror1PinIn) < 0 ) {
        digitalWrite(Mirror1PinHighOut, HIGH);
        digitalWrite(Mirror1PinLowOut, HIGH);
    } else {
        digitalWrite(Mirror1PinHighOut, HIGH);
        digitalWrite(Mirror1PinLowOut, LOW);
    }
    if( read(Mirror2PinIn) > 0 ) {
        digitalWrite(Mirror2PinLowOut, LOW);
        digitalWrite(Mirror2PinHighOut, LOW);
    } else if( read(Mirror2PinIn) < 0 ) {
        digitalWrite(Mirror2PinHighOut, HIGH);
        digitalWrite(Mirror2PinLowOut, HIGH);
    } else {
        digitalWrite(Mirror2PinHighOut, HIGH);
        digitalWrite(Mirror2PinLowOut, LOW);
    }
}


void movement(const bool down) {
    debug(F("movement(%s)"), down ? "down" : "up");
    digitalWrite(MagneticswitchPinOut, LOW);
    if( (down && !preferences.invert) || (!down && preferences.invert) ) {
        digitalWrite(MagneticswitchPinOut, LOW);
        digitalWrite(Mirror1PinLowOut, LOW);
        digitalWrite(Mirror1PinHighOut, LOW);
        digitalWrite(Mirror2PinHighOut, HIGH);
        digitalWrite(Mirror2PinLowOut, HIGH);
    } else {
        digitalWrite(MagneticswitchPinOut, LOW);
        digitalWrite(Mirror1PinHighOut, HIGH);
        digitalWrite(Mirror1PinLowOut, HIGH);
        digitalWrite(Mirror2PinLowOut, LOW);
        digitalWrite(Mirror2PinHighOut, LOW);
    }
}


void movement_stop() {
    debug(F("movement_stop()"));
    digitalWrite(Mirror1PinHighOut, HIGH);
    digitalWrite(Mirror1PinLowOut, LOW);
    digitalWrite(Mirror2PinHighOut, HIGH);
    digitalWrite(Mirror2PinLowOut, LOW);
    digitalWrite(MagneticswitchPinOut, HIGH);
}


void handle_led() {
    static unsigned long lastChange = 0;
    unsigned long now = millis();
    if( led == ready ) {
        digitalWrite(LedPin, HIGH);
    } else if( led == aborted ) {
        digitalWrite(LedPin, LOW);
    } else if( (led == moving && now - lastChange >= LedBlinkFast)
            || (led == almir && now - lastChange >= LedBlinkSlow) ) {
        digitalWrite(LedPin, !digitalRead(LedPin));
        lastChange = now;
    }
}


void set_led(const enum State l) {
    static enum State lastLed = led;
    debug(F("set_led(%d)"), l);
    if( l == restore ) {
        led = lastLed;
    } else {
        lastLed = led;
        led = l;
    }
    handle_led();
}


bool lowering() {
    debug(F("lowering() from lowered=%d"), lowered);
    const unsigned long t0 = millis();
    movement(true);
    set_led(moving);
    while( millis() - t0 < preferences.loweringDuration - lowered ) {
        background();
        if( input() || !reverse() ) {       // abort ALMIR
            debug(F("lowering(): abort"));
            movement_stop();
            lowered += millis() - t0;
            set_state(aborted);
            heightening();
            return false;
        }
    }
    movement_stop();
    set_led(restore);
    lowered = preferences.loweringDuration;
    return true;
}


bool heightening() {
    debug(F("heightening() from lowered=%d"), lowered);
    const unsigned long t0 = millis();
    movement(false);
    set_led(moving);
    while( millis() - t0 < lowered ) {
        background();
        if( reverse() ) {       // continue ALMIR
            debug(F("heightening(): continue"));
            movement_stop();
            lowered -= millis() - t0;
            do_almir();
            return false;
        }
    }
    movement_stop();
    set_led(restore);
    lowered = 0;
    return true;
}


void nick(const int count) {
    debug(F("nick(%d)"), count);
    for( unsigned int i = 0; i < count; i++ ) {
        movement(false);
        delayBackground(500);
        movement(true);
        delayBackground(500);
        movement_stop();
    }
}



// main program

void background() {
    #ifdef DEBUG
    SerialUSB.refresh();
    #endif
    handle_led();
}


void delayBackground(const unsigned long duration) {
    const unsigned long t0 = millis();
    while( millis() - t0 < duration ) {
        background();
    }
}


void do_almir() {
    debug(F("do_almir()"));
    set_led(almir);

    if( !lowering() ) {
        debug(F("do_almir(): abort"));
        return;
    }

    unsigned int lastCount, currentCount=reverse_count();
    do {    // adjustments during ALMIR
        background();
        forward_inputs();
        if( input_down() && reverse_gear() ) {        // record down adjustments
            const unsigned long t0 = millis();
            debug(F("do_almir(): record down adjustments"));
            while( input_down() && reverse_gear() ) {
                background();
            }
            movement_stop();
            preferences.loweringDuration += millis() - t0;
            lowered = preferences.loweringDuration;
            debug(F("do_almir(): new loweringDuratin=%d"), preferences.loweringDuration);
            preferences_save();
        } else if( input_up() && reverse_gear() ) {   // record up adjustments
            const unsigned long t0 = millis();
            debug(F("do_almir(): record up adjustments"));
            while( input_up() && reverse_gear() ) {
                background();
            }
            movement_stop();
            preferences.loweringDuration -= millis() - t0;
            lowered = preferences.loweringDuration;
            debug(F("do_almir(): new loweringDuratin=%d"), preferences.loweringDuration);
            preferences_save();
        }
        lastCount = currentCount;
        currentCount = reverse_count();
    } while( 0 < currentCount && currentCount < 6 );
    movement_stop();    // left/right

    if( currentCount != 0 ) {
        // wait for HeighteningTimeout to capture all programming inputs
        debug(F("do_almir(): wait for currentCount=0"));
        while( currentCount != 0 ) {
            background();
            lastCount = currentCount;
            currentCount = reverse_count();
        }
    }
    debug(F("do_almir(): handle last count=%d"), lastCount);

    if( lastCount == 6 ) {              // toggle doubleReverse
        preferences.doubleReverse = !preferences.doubleReverse;
        preferences_save();
        if( !preferences.doubleReverse ) {  // yes --> no (mode 1)
            debug(F("do_almir(): doubleReverse set to false"));
            nick(1);
        } else {                            // no --> yes (mode 2)
            debug(F("do_almir(): doubleReverse set to true"));
            nick(2);
        }

    } else if( lastCount == 7 ) {       // toggle invert
        preferences.invert = !preferences.invert;
        preferences_save();
        if( !preferences.invert ) {         // yes --> no
            debug(F("do_almir(): invert set to false"));
            nick(3);
        } else {                            // no --> yes
            debug(F("do_almir(): invert set to true"));
            nick(4);
        }
    } else if( lastCount == 8 ) {
        preferences_reset();
    }

    heightening();
}



void setup() {
    #ifdef DEBUG
    SerialUSB.begin();
    SerialUSB.delay(1000);
    #endif
    debug(F("\n\n\rsetup()"));

    // mirror outputs and deactivate each
    pinMode(Mirror1PinHighOut, OUTPUT);
    digitalWrite(Mirror1PinHighOut, HIGH);
    pinMode(Mirror1PinLowOut, OUTPUT);
    digitalWrite(Mirror1PinLowOut, LOW);
    pinMode(Mirror2PinHighOut, OUTPUT);
    digitalWrite(Mirror2PinHighOut, HIGH);
    pinMode(Mirror2PinLowOut, OUTPUT);
    digitalWrite(Mirror2PinLowOut, LOW);

    // mirror inputs are analogue
    // logical in-/outputs
    pinMode(BlockPinHighIn, INPUT);
    pinMode(BlockPinLowIn, INPUT);
    pinMode(MagneticswitchPinOut, OUTPUT);
    digitalWrite(MagneticswitchPinOut, HIGH);
    pinMode(LedPin, OUTPUT);
    digitalWrite(LedPin, LOW);

    preferences_load();
    debug(F("setup() completed, start loop()"));
}



void loop() {
    if( reverse() ) {
        do_almir();
    }
    forward_inputs();
    background();
}
